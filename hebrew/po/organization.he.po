msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2008-06-17 13:57+0200\n"
"Last-Translator: Oz Nahum <nahumoz@gmail.com>\n"
"Language-Team: Hebrew <he@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "חבר"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "מנהל"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
#| msgid "Release Management"
msgid "Stable Release Manager"
msgstr "הנהלת השחרור"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "אשף"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "יושב ראש"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "עוזר"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "מזכיר"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "קצינים"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:87
msgid "Distribution"
msgstr "הפצה"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:229
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:232
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "יחסי ציבור"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:300
msgid "Support and Infrastructure"
msgstr "תמיכה ותשתית"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "מנהיג"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "וועדה טכנית"

#: ../../english/intro/organization.data:82
msgid "Secretary"
msgstr "מזכיר"

#: ../../english/intro/organization.data:90
msgid "Development Projects"
msgstr "מיזמי פיתוח"

#: ../../english/intro/organization.data:91
msgid "FTP Archives"
msgstr "ארכיוני FTP"

#: ../../english/intro/organization.data:93
#, fuzzy
#| msgid "FTP Master"
msgid "FTP Masters"
msgstr "מנהל ה-FTP"

#: ../../english/intro/organization.data:99
msgid "FTP Assistants"
msgstr "סייעי FTP"

#: ../../english/intro/organization.data:104
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:108
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:110
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:114
msgid "Individual Packages"
msgstr "חבילות עצמאיות"

#: ../../english/intro/organization.data:115
msgid "Release Management"
msgstr "הנהלת השחרור"

#: ../../english/intro/organization.data:117
msgid "Release Team"
msgstr "צוות השחרור"

#: ../../english/intro/organization.data:130
msgid "Quality Assurance"
msgstr "אבטחת איכות"

#: ../../english/intro/organization.data:131
msgid "Installation System Team"
msgstr "צוות התקנת המערכת"

#: ../../english/intro/organization.data:132
msgid "Release Notes"
msgstr "הודעות שחרור"

#: ../../english/intro/organization.data:134
msgid "CD Images"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Production"
msgstr "הפקה"

#: ../../english/intro/organization.data:144
msgid "Testing"
msgstr "בחינה"

#: ../../english/intro/organization.data:146
#, fuzzy
#| msgid "Support and Infrastructure"
msgid "Autobuilding infrastructure"
msgstr "תמיכה ותשתית"

#: ../../english/intro/organization.data:148
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:156
#, fuzzy
#| msgid "System Administration"
msgid "Buildd administration"
msgstr "ניהול מערכת"

#: ../../english/intro/organization.data:175
msgid "Documentation"
msgstr "תיעוד"

#: ../../english/intro/organization.data:180
msgid "Work-Needing and Prospective Packages list"
msgstr "עבודה נדרשת והצעות לאריזת תוכנה"

#: ../../english/intro/organization.data:183
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:184
msgid "Ports"
msgstr ""

#: ../../english/intro/organization.data:219
msgid "Special Configurations"
msgstr "הגדרות מיוחדות"

#: ../../english/intro/organization.data:222
msgid "Laptops"
msgstr "מחשבים ניידים"

#: ../../english/intro/organization.data:223
msgid "Firewalls"
msgstr "חומות אש"

#: ../../english/intro/organization.data:224
msgid "Embedded systems"
msgstr ""

#: ../../english/intro/organization.data:237
msgid "Press Contact"
msgstr "איש קשר לעיתונות"

#: ../../english/intro/organization.data:239
msgid "Web Pages"
msgstr "דפי רשת"

#: ../../english/intro/organization.data:249
msgid "Planet Debian"
msgstr "פלאנט דביאן"

#: ../../english/intro/organization.data:254
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:258
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:266
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:272
msgid "Events"
msgstr "אירועים"

#: ../../english/intro/organization.data:278
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "וועדה טכנית"

#: ../../english/intro/organization.data:285
msgid "Partner Program"
msgstr "שותפים לתוכנית"

#: ../../english/intro/organization.data:290
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:303
msgid "User support"
msgstr "תמיכת משתמשים"

#: ../../english/intro/organization.data:370
msgid "Bug Tracking System"
msgstr "מערכת מעקב באגים"

#: ../../english/intro/organization.data:375
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "ניהול רשימות תפוצה וארכיון רשימות תפוצה"

#: ../../english/intro/organization.data:383
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:389
msgid "Debian Account Managers"
msgstr "מנהלי חשבונות דביאן"

#: ../../english/intro/organization.data:393
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:394
msgid "Keyring Maintainers (PGP and GPG)"
msgstr ""

#: ../../english/intro/organization.data:397
msgid "Security Team"
msgstr "צוות אבטחה"

#: ../../english/intro/organization.data:409
msgid "Consultants Page"
msgstr ""

#: ../../english/intro/organization.data:414
msgid "CD Vendors Page"
msgstr ""

#: ../../english/intro/organization.data:417
msgid "Policy"
msgstr "מדיניות"

#: ../../english/intro/organization.data:422
msgid "System Administration"
msgstr "ניהול מערכת"

#: ../../english/intro/organization.data:423
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:432
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:433
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:434
msgid "Mirrors"
msgstr ""

#: ../../english/intro/organization.data:441
msgid "DNS Maintainer"
msgstr ""

#: ../../english/intro/organization.data:442
msgid "Package Tracking System"
msgstr ""

#: ../../english/intro/organization.data:444
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:450
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:453
#, fuzzy
#| msgid "System Administration"
msgid "Salsa administrators"
msgstr "ניהול מערכת"

#: ../../english/intro/organization.data:457
msgid "Alioth administrators"
msgstr ""

#: ../../english/intro/organization.data:470
msgid "Debian for children from 1 to 99"
msgstr ""

#: ../../english/intro/organization.data:473
msgid "Debian for medical practice and research"
msgstr ""

#: ../../english/intro/organization.data:476
msgid "Debian for education"
msgstr ""

#: ../../english/intro/organization.data:481
msgid "Debian in legal offices"
msgstr ""

#: ../../english/intro/organization.data:485
msgid "Debian for people with disabilities"
msgstr ""

#: ../../english/intro/organization.data:489
msgid "Debian for science and related research"
msgstr ""

#: ../../english/intro/organization.data:492
msgid "Debian for astronomy"
msgstr ""

#~ msgid "Testing Security Team"
#~ msgstr "צוות אבטחה להפצת הבחינה"

#~ msgid "Marketing Team"
#~ msgstr "צוות שיווק"

#~ msgid "Handhelds"
#~ msgstr "מחשבי כף יד"

#~ msgid "APT Team"
#~ msgstr "צוות APT"

#~ msgid "Vendors"
#~ msgstr "ספקים"

#~ msgid "Release Team for ``stable''"
#~ msgstr "צוות השחרור להפצה ה'יציבה'"

#~ msgid "Custom Debian Distributions"
#~ msgstr "הפצות דביאן מותאמות אישית"

#~ msgid "Publicity"
#~ msgstr "יחסי ציבור"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "צוות התקנת המערכת"
